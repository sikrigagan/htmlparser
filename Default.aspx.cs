﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Web.UI;
using Newtonsoft.Json;
using System.Web.UI.WebControls;
using HtmlAgilityPack;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    if(!IsPostBack)
    {
        Label1.Text = "Train Running Status | HTML Data Parsing & JSON Export";
        Page.Title = "Train Running Status | Indian Railways";
    }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //creating request URL
        string base_url = "http://railenquiry.in/runningstatus/";
        string req_url = base_url + tno_text.Text + "/" + date_txt.Text;
        
        //Http get request
        try
        {
          HttpWebRequest request = (HttpWebRequest)WebRequest.Create(req_url); //http request
          HttpWebResponse response = (HttpWebResponse)request.GetResponse(); //http response
          if (response.StatusCode == HttpStatusCode.OK) //status check
          {
              Stream rec_stream = response.GetResponseStream(); 
              StreamReader r_stream;
              r_stream = new StreamReader(rec_stream);
              string data = r_stream.ReadToEnd();
              response.Close();
              r_stream.Close();
              var htmlweb = new HtmlWeb(); //HtmlAgilityPack
              var doc = new HtmlAgilityPack.HtmlDocument();
              doc.LoadHtml(data); //loaded stream of data to HtmlAgilityPack HTML Document Load to extract out running status.
              runningstatus_lbl.Visible = true;
              runningstatus_lbl.Text = " ";
              getjson_btn.Visible = true;
              string train_details = doc.DocumentNode.SelectSingleNode("//title").InnerText.ToString(); //todisplay train info
              traininfo_lbl.Visible = true;
              traininfo_lbl.Text = train_details;
              foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table[@class='table table-striped table-vcenter table-bordered table-responsive']"))
              {
                  runningstatus_lbl.Text += table.OuterHtml; //outerhtml to display results on same asp.net webpage
                  msg_lbl.Text += table.InnerText; //innertext to export json without html element tags.
              }
          }
        }
            
        catch
        {
            msg_lbl.Visible = true;
            msg_lbl.Text = "Something went wrong. Please try again!";
        }
    }

    public class JsonExport
    {
        public string train_name { get; set; }
        public IList<string> train_runningstatus { get; set; }
    }
    
    protected void getjson_btn_Click(object sender, EventArgs e)
    {
        try
        {
            runningstatus_lbl.Visible = false;
            string str = msg_lbl.Text;
            string tinfo = traininfo_lbl.Text;
        JsonExport res = new JsonExport
        {
            train_name = tinfo.Replace(" Live Train Running Status", ""),
            train_runningstatus = str.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).ToList(),
        };

        string json = JsonConvert.SerializeObject(res, Formatting.Indented);
        File.WriteAllText(@Server.MapPath("~/jsonexports/" + tno_text.Text + "_" + date_txt.Text + ".json"), json);
        getjson_btn.Visible = false;
        jsonlabel.Visible = true;
        jsonlabel.Text = "JSON File exported successfully with file name: " + tno_text.Text + "_" + date_txt.Text + ".json";
        rst_btn.Visible = true;
        }
        catch
        {
            jsonlabel.Visible = true;
            jsonlabel.Text = "Something went wrong parsng JSON. Please try again!";
        }
        
    }



    protected void rst_btn_Click(object sender, EventArgs e)
    {
        Server.Transfer("~/Default.aspx");
    }
}