﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><hr />
        <br />
        Enter Train Number:
        <asp:TextBox ID="tno_text" runat="server" MaxLength="6" TextMode="Number"></asp:TextBox>
        <br />
        Enter Date:
        <asp:TextBox ID="date_txt" runat="server"></asp:TextBox>
&nbsp;Enter date in format: <strong>YYYY-MM-DD</strong> <em>Eg.</em> <strong>2015-07-23</strong><br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Get Running Status" />
&nbsp;<asp:Label ID="msg_lbl" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <hr />
        <br />
        <asp:Label ID="traininfo_lbl" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:Label ID="runningstatus_lbl" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:Button ID="getjson_btn" runat="server" OnClick="getjson_btn_Click" Text="JSON" Visible="False" />
&nbsp;<asp:Label ID="jsonlabel" runat="server" Text="Label" Visible="False"></asp:Label>
&nbsp;&nbsp;
        <asp:Button ID="rst_btn" runat="server" OnClick="rst_btn_Click" Text="Reset" Visible="False" />
        <br />
    
    </div>
    </form>
</body>
</html>
